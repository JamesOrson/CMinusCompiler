package Compiler.Scanner;

import java.io.IOException;

/**
  * Provides an interface for a Scanner object.
  *
  * @author James Osborne and Jeremy Tiberg
  * File: IScanner.java
  * Created:  29 Jan 2018
  * Description: This interface provides two method signatures
  * it will require an implementation to provide.
  **/

public interface IScanner {
    public Token getNextToken() throws IOException;
    public Token viewNextToken();
}
