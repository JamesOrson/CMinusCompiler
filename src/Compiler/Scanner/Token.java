package Compiler.Scanner;

import static Compiler.Scanner.TokenType.*;

/**
  * Provides a class for the Token object. 
  * for the types of tokens the CMinusScanner will support.
  * 
  * @author James Osborne and Jeremy Tiberg
  * File: Token.java
  * Created:  29 Jan 2018
  * Description: The Token object simply provides 
  * accessor and mutator methods for Token fields.
  **/

public class Token {
    private TokenType tokenType;
    private Object tokenData;
    private int lineNumber;
    
    public Token() {
        this(DEFAULT_TOKEN_TYPE, null, -1);
    }
    
    public Token(TokenType tokenType, Object tokenData, int lineNumber) {
        this.tokenType = tokenType;
        this.tokenData = tokenData;
        this.lineNumber = lineNumber;
    }
    
    /**
      * Returns the type of this token
      * 
      * @return the type of the token
      **/
    public TokenType getTokenType() {
        return this.tokenType;
    }
    
    /**
      * Returns the data of this token
      * 
      * @return the data of the token
      **/
    public Object getTokenData() {
        return this.tokenData;
    }
    
    /**
      * Returns the line number of this token
      * 
      * @return the line number of the token
      **/
    public int getLineNumber() {
        return this.lineNumber;
    }
    
    /**
      * Sets the type for the token
      * 
      * @param tokenType the type to set the token to
      **/
    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }
    
    /**
      * Sets the data for the token
      * 
      * @param tokenData the data to set in the token
      **/
    public void setTokenData(Object tokenData) {
        this.tokenData = tokenData;
    }
    
    /**
      * Sets the line number for the token
      * 
      * @param lineNumber the line number to set in the token
      **/
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }
    
    /**
      * Sets the fields of the token
      * 
      * @param tokenType the type to set the token to
      * @param tokenData the data to set in the token
      **/
    public void setTokenFields(TokenType tokenType, Object tokenData, int lineNumber) {
        setTokenType(tokenType);
        setTokenData(tokenData);
        setLineNumber(lineNumber);
    }
}
