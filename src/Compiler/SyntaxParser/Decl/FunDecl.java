package Compiler.SyntaxParser.Decl;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Other.Param;
import static Compiler.CMinusCompiler.globalHash;
import Compiler.SyntaxParser.Statement.CurlyStatement;
import java.util.ArrayList;
import java.util.HashMap;
import lowlevel.*;
import static lowlevel.Data.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: FunDecl.java
  **/
public class FunDecl extends Decl {
    private ArrayList<Param> paramList;
    private CurlyStatement compoundStatement;
    
    public FunDecl() {
        this(null, null, new ArrayList<Param>(), null);
    }
    
    public FunDecl(String typeSpecifier,
                   String id, 
                   ArrayList<Param> paramList, 
                   CurlyStatement compoundStatement) {
        this.typeSpecifier = typeSpecifier;
        this.id = id;
        this.paramList = paramList;
        this.compoundStatement = compoundStatement;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Fun Decl {");
        
        printHelper(childIndent, this.typeSpecifier);
        printHelper(childIndent, this.id);
        printHelper(childIndent, "(");
        if (this.paramList.isEmpty()) {
            printHelper(childIndent, "void");
        } else {
            this.paramList.get(0).print(childIndent);
            for (int i = 1; i < this.paramList.size(); ++i) {
                printHelper(childIndent, ",");
                this.paramList.get(i).print(childIndent);
            }
        }
        printHelper(childIndent, ")");
        this.compoundStatement.print(childIndent);
        
        printHelper(indent, "}");
    }
    
    public CodeItem genLLCode(HashMap symbolTable) {
        if (globalHash.containsKey(id)) {
            throw new LowLevelException("Function '" + id + "' already declared");
        }
        globalHash.put(id, id);
        
        int returnType = typeSpecifier == "int" ? TYPE_INT : TYPE_VOID;
        Function function = new Function(returnType, id);
        
        FuncParam firstFuncParam = null;
        HashMap funcSymbolTable = function.getTable();
        
        if (paramList.size() > 0) {
            firstFuncParam = paramList.get(0).genLLCode();
            FuncParam currFuncParam = firstFuncParam;
            FuncParam nextFuncParam = null;
            String paramName = firstFuncParam.getName();
            
            if (funcSymbolTable.containsKey(paramName) ||
                globalHash.containsKey(paramName)) {
                throw new LowLevelException("Two params used in '" + id +
                    "' with name '" + paramName + "'");
            }
            funcSymbolTable.put(currFuncParam.getName(), function.getNewRegNum());

            for (int i = 1; i < paramList.size(); ++i) {
                nextFuncParam = paramList.get(i).genLLCode();
                paramName = nextFuncParam.getName();
                
                if (funcSymbolTable.containsKey(paramName) ||
                    globalHash.containsKey(paramName)) {
                    throw new LowLevelException("Two params used in '" + id +
                        "' with name '" + paramName + "'");
                }
                funcSymbolTable.put(nextFuncParam.getName(), function.getNewRegNum());
                currFuncParam.setNextParam(nextFuncParam);
                currFuncParam = nextFuncParam;
            }
        }
        
        function.setFirstParam(firstFuncParam);
        function.createBlock0();
        function.setCurrBlock(function.getFirstBlock());
        function.appendToCurrentBlock(new BasicBlock(function));
        function.setCurrBlock(function.getCurrBlock().getNextBlock());
        BasicBlock returnBlock = function.genReturnBlock();
        if (compoundStatement != null) {
            BasicBlock funBlock = new BasicBlock(function);
            compoundStatement.genLLCode(function, returnBlock, funBlock);
        }
        function.appendBlock(returnBlock);
        BasicBlock unconnectedBlock = function.getFirstUnconnectedBlock();
        if (unconnectedBlock != null) {
            function.appendBlock(unconnectedBlock);
            function.setFirstUnconnectedBlock(null);
        }
        
        return function;
    }
}
