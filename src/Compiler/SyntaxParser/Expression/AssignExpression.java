package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import lowlevel.BasicBlock;
import lowlevel.Function;
import lowlevel.Operand;
import static lowlevel.Operand.OperandType.*;
import lowlevel.Operation;
import static lowlevel.Operation.OperationType.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: AssignExpression.java
  **/
public class AssignExpression extends Expression {
    private VarExpression leftHandSide;
    private Expression rightHandSide;
    
    public AssignExpression(VarExpression leftHandSide, Expression rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Assign Expression {");
        
        this.leftHandSide.print(childIndent);
        printHelper(childIndent, "=");
        this.rightHandSide.print(childIndent);
        
        printHelper(indent, "}");
    }
    
    @Override
    public Operation genLLCode(Function function) {
        BasicBlock currBlock = function.getCurrBlock();
        Operand destOperand = leftHandSide.genLLCode(function).getSrcOperand(0);
        Operand srcOperand = null;
        Operation rightOperation = rightHandSide.genLLCode(function);
        if (rightHandSide instanceof CallExpression) {
            srcOperand = new Operand(MACRO, "RetReg");
        } else {
            srcOperand = rightOperation.getDestOperand(0);
        }
        
        Operation operation = null;
        if (destOperand.getType() == STRING) {
            operation = new Operation(STORE_I, currBlock);
            operation.setSrcOperand(0, srcOperand);
            operation.setSrcOperand(1, destOperand);
            operation.setSrcOperand(2, new Operand(INTEGER, 0));
            
            currBlock.appendOper(operation);
        } else {
            operation = new Operation(ASSIGN, currBlock);
            operation.setDestOperand(0, destOperand);
            operation.setSrcOperand(0, srcOperand);

            currBlock.appendOper(operation);
        }
        return operation;
    }
}
