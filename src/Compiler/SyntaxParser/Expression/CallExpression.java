package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import java.util.ArrayList;
import lowlevel.*;
import static lowlevel.Operand.OperandType.*;
import static lowlevel.Operation.OperationType.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: CallExpression.java
  **/
public class CallExpression extends Expression {
    private String id;
    private ArrayList<Expression> args;
    
    public CallExpression(String id, ArrayList<Expression> args) {
        this.id = id;
        this.args = args;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Call Expression {");
        
        printHelper(childIndent, id);
        printHelper(childIndent, "(");
        if (!this.args.isEmpty()) {
            args.get(0).print(childIndent);
            for (int i = 1; i < this.args.size(); ++i) {
                printHelper(childIndent, ",");
                args.get(i).print(childIndent);
            }
        }
        printHelper(childIndent, ")");
        
        printHelper(indent, "}");
    }
    
    public Operation genLLCode(Function function) {
        BasicBlock currBlock = function.getCurrBlock();
        
        for (int i = 0; i < args.size(); ++i) {
            Expression arg = args.get(i);
            Operation argOperation = new Operation(PASS, currBlock);
            Operand argOperand = arg.genLLCode(function).getDestOperand(0);
            
            if (argOperand.getType() == STRING) {
                Operation loadOperation = new Operation(LOAD_I, currBlock);
                Operand destOperand = new Operand(REGISTER, function.getNewRegNum());
                loadOperation.setDestOperand(0, destOperand);
                loadOperation.setSrcOperand(0, argOperand);
                loadOperation.setSrcOperand(1, new Operand(INTEGER, 0));
                currBlock.appendOper(loadOperation);
                argOperation.setSrcOperand(0, destOperand);
            } else {
                argOperation.setSrcOperand(0, argOperand);
            }
            
            argOperation.addAttribute(new Attribute("PARAM_NUM", Integer.toString(i)));
            currBlock.appendOper(argOperation);
        }
        
        Operation callOperation = new Operation(CALL, currBlock);
        Operand callOperand = new Operand(STRING, id);
        callOperation.addAttribute(new Attribute("numParams", Integer.toString(args.size())));
        
        callOperation.setSrcOperand(0, callOperand);
        currBlock.appendOper(callOperation);
        
        return callOperation;
    }
}
