package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import lowlevel.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Expression.java
  **/
public class Expression {
    protected boolean hasParens;
    private Expression expression;
    protected Object regNum;
    
    public Expression() {
        this(false, null);
    }
    
    public Expression(boolean hasParens, Expression expression) {
        this.hasParens = hasParens;
        this.expression = expression;
        this.regNum = -1;
    }
    
    public void setHasParens(boolean hasParens) {
        this.hasParens = hasParens;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Expression {");
        
        if (this.expression != null) {
            if (this.hasParens) {
                printHelper(childIndent, "(");
            }
            this.expression.print(childIndent);
            if (this.hasParens) {
                printHelper(childIndent, ")");
            }
        }
        
        printHelper(indent, "}");
    }
    
    public Operation genLLCode(Function function) {
        throw new LowLevelException("Should not call genLLCode on generic expression");
    }
}
