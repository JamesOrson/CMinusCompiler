package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import lowlevel.*;
import static lowlevel.Operand.OperandType.*;
import lowlevel.Operation;
import static lowlevel.Operation.OperationType.ASSIGN;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Numexpression.java
  **/
public class NumExpression extends Expression {
    private Integer num;
    
    public NumExpression(int num) {
        this.num = num;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Num Expression {");
        
        printHelper(childIndent, num.toString());
        
        printHelper(indent, "}");
    }
    
    @Override
    public Operation genLLCode(Function function) {
        BasicBlock currBlock = function.getCurrBlock();
        
        Operation integerOperation = new Operation(ASSIGN, currBlock);
        Operand srcIntegerOperand = new Operand(INTEGER, num);
        Operand registerOperand = new Operand(REGISTER, function.getNewRegNum());
        integerOperation.setSrcOperand(0, srcIntegerOperand);
        integerOperation.setDestOperand(0, registerOperand);
        currBlock.appendOper(integerOperation);
        
        Operation registerOperation = new Operation(ASSIGN, currBlock);
        registerOperation.setSrcOperand(0, registerOperand);
        registerOperation.setDestOperand(0, registerOperand);
        
        return registerOperation;
    }
}
