package Compiler.SyntaxParser.Expression;

import static Compiler.CMinusCompiler.globalHash;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import lowlevel.*;
import static lowlevel.Operand.OperandType.*;
import static lowlevel.Operation.OperationType.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: VarExpression.java
  **/
public class VarExpression extends Expression {
    private String id;
    private boolean hasBrackets;
    private Expression bracketExpression;
    
    public VarExpression(String id) {
        this(id, false, null);
    }
    
    public VarExpression(String id, boolean hasBrackets, Expression bracketExpression) {
        this.id = id;
        this.hasBrackets = hasBrackets;
        this.bracketExpression = bracketExpression;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Var Expression {");
        
        printHelper(childIndent, id);
        if (this.hasBrackets) {
            printHelper(childIndent, "[");
            this.bracketExpression.print(childIndent);
            printHelper(childIndent, "]");
        }
        
        printHelper(indent, "}");
    }
    
    @Override
    public Operation genLLCode(Function function) {
        BasicBlock currBlock = function.getCurrBlock();
        Operation operation = new Operation(ASSIGN, currBlock);
        
        if (!function.getTable().containsKey(id)) {
            if (!globalHash.containsKey(id)) {
                throw new LowLevelException("Variable '" + id + "' not defined");
            } else {
                Operand srcOperand = new Operand(STRING, id);
                operation.setSrcOperand(0, srcOperand);
                operation.setDestOperand(0, srcOperand);
                return operation;
            }
        } else {
            regNum = function.getTable().get(id);
        }
        
        Operand srcOperand = new Operand(REGISTER, regNum);
        operation.setSrcOperand(0, srcOperand);
        operation.setDestOperand(0, srcOperand);
        
        return operation;
    }
}
