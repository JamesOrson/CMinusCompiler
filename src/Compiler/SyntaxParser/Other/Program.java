package Compiler.SyntaxParser.Other;

import static Compiler.CMinusCompiler.globalHash;
import java.util.ArrayList;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Decl.*;
import lowlevel.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Program.java
  **/
public class Program {
    private ArrayList<Decl> declarationList;
    
    public Program() {
        this(new ArrayList<Decl>());
    }
    
    public Program(ArrayList<Decl> declarationList) {
        this.declarationList = declarationList;
    }
    
    public void print() {
        int indent = 0;
        int childIndent = indent + 4;
        
        printHelper(indent, "Program {");
        
        for (Decl decl : this.declarationList) {
            decl.print(childIndent);
        }
        
        printHelper(indent, "}");
    }
    
    public CodeItem genLLCode() {
        Decl currDecl = declarationList.get(0);
        CodeItem firstCodeItem = currDecl.genLLCode(globalHash);
        CodeItem currCodeItem = firstCodeItem;
        CodeItem nextCodeItem = null;
        if (currDecl instanceof VarDecl) { 
            Data data = (Data) currCodeItem;
            globalHash.put(data.getName(), data.getName());
        }
        
        for (int i = 1; i < declarationList.size(); ++i) {
            currDecl = declarationList.get(i);
            nextCodeItem = currDecl.genLLCode(globalHash);
            
            currCodeItem.setNextItem(nextCodeItem);
            currCodeItem = nextCodeItem;
            
            if (currDecl instanceof VarDecl) {
                Data data = (Data) currCodeItem;
                globalHash.put(data.getName(), data.getName());
            }
        }
        
        return firstCodeItem;
    }
}
