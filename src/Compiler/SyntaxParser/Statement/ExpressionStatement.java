package Compiler.SyntaxParser.Statement;

import Compiler.SyntaxParser.Statement.Statement;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Expression.*;
import lowlevel.BasicBlock;
import lowlevel.Function;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: ExpressionStatement.java
  **/
public class ExpressionStatement extends Statement {
    private Expression expression;
    
    public ExpressionStatement() {
        this(null);
    }
    
    public ExpressionStatement(Expression expression) {
        this.expression = expression;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Expression Statement {");
        
        if (this.expression != null) {
            this.expression.print(childIndent);
        }
        printHelper(childIndent, ";");
        
        printHelper(indent, "}");
    }
    
    @Override
    public void genLLCode(Function function) {
        genLLCode(function);
    }
    
    @Override
    public void genLLCode(Function function, BasicBlock returnBlock) {
        genLLCode(function, returnBlock, null);
    }
    
    @Override
    public void genLLCode(Function function, BasicBlock returnBlock, BasicBlock basicBlock) {
        expression.genLLCode(function);
    }
}
