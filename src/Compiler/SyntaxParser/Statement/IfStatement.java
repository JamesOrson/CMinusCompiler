package Compiler.SyntaxParser.Statement;

import Compiler.SyntaxParser.Statement.Statement;
import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import Compiler.SyntaxParser.Expression.*;
import lowlevel.*;
import lowlevel.Function;
import static lowlevel.Operand.OperandType.*;
import static lowlevel.Operation.OperationType.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: IfStatement.java
  **/
public class IfStatement extends Statement {
    private Expression ifExpression;
    private Statement ifStatement;
    private Statement elseStatement;
    
    public IfStatement() {
        this(null, null, null);
    }
    
    public IfStatement(Expression ifExpression, Statement ifStatement) {
        this(ifExpression, ifStatement, null);
    }
    
    public IfStatement(Expression ifExpression, Statement ifStatement, Statement elseStatement) {
        this.ifExpression = ifExpression;
        this.ifStatement = ifStatement;
        this.elseStatement = elseStatement;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "If Statement {");
        
        printHelper(childIndent, "if");
        printHelper(childIndent, "(");
        this.ifExpression.print(childIndent);
        printHelper(childIndent, ")");
        this.ifStatement.print(childIndent);
        if (this.elseStatement != null) {
            printHelper(childIndent, "else");
            this.elseStatement.print(childIndent);
        }
        
        printHelper(indent, "}");
    }
    
    @Override
    public void genLLCode(Function function) {
        genLLCode(function, null);
    }
    
    @Override
    public void genLLCode(Function function, BasicBlock returnBlock) {
        genLLCode(function, returnBlock, null);
    }
        
    @Override
    public void genLLCode(Function function, BasicBlock returnBlock, BasicBlock basicBlock) {
        BasicBlock currBlock = function.getCurrBlock();
        //Declare 2-3 blocks. 2 if no else, 3 if else
        BasicBlock thenBlock = new BasicBlock(function);
        BasicBlock postBlock = new BasicBlock(function);
        
        BasicBlock elseBlock = null;
        
        if (elseStatement != null) {
            elseBlock = new BasicBlock(function);
        }
        
        //Call gen code on the expression
        Operation ifOperation = ifExpression.genLLCode(function);
        
        //Make a branch, depending on else or not
        Operation branchOperation = new Operation(BEQ, currBlock);
        
        if (ifExpression instanceof CallExpression) {
            branchOperation.setSrcOperand(0, new Operand(MACRO, "RetReg"));
        } else {
            branchOperation.setSrcOperand(0, ifOperation.getDestOperand(0));
        }
        branchOperation.setSrcOperand(1, new Operand(INTEGER, 0));
        if (elseStatement == null) {
            branchOperation.setSrcOperand(2, new Operand(BLOCK, postBlock.getBlockNum()));
        } else {
            branchOperation.setSrcOperand(2, new Operand(BLOCK, elseBlock.getBlockNum()));
        }
        currBlock.appendOper(branchOperation);
        
        //Append “Then” with appendToCurrentBlock()
        thenBlock.setNextBlock(currBlock.getNextBlock());
        function.appendToCurrentBlock(thenBlock);
        //currentBlock = thenBlock
        function.setCurrBlock(thenBlock);
        currBlock = thenBlock;
        //GenCode on thenBlock
        ifStatement.genLLCode(function);
        currBlock = function.getCurrBlock();

        Operation jumpOperation = new Operation(JMP, currBlock);
        Operand jumpOperand = new Operand(BLOCK, postBlock.getBlockNum());
        jumpOperation.setSrcOperand(0, jumpOperand);
        currBlock.appendOper(jumpOperation);
        
        //Append postBlock
        function.appendToCurrentBlock(postBlock);
        if (elseBlock != null) {
            //currentBlock = elseBlock (8 through 11 only if else exists)
            function.setCurrBlock(elseBlock);
            currBlock = elseBlock;
            //GenCode on elseBlock (8 through 11 only if else exists)
            elseStatement.genLLCode(function, null, elseBlock);
            //Jump to post (8 through 11 only if else exists)
            currBlock = function.getCurrBlock();
            jumpOperation = new Operation(JMP, currBlock);
            jumpOperand = new Operand(BLOCK, postBlock.getBlockNum());
            jumpOperation.setSrcOperand(0, jumpOperand);
            currBlock.appendOper(jumpOperation);
            //Append elseBlock to unconnectedChain (8 through 11 only if else exists)
            function.appendUnconnectedBlock(elseBlock);
        }
        //currentBlock = postBlock
        function.setCurrBlock(postBlock);
    }
}
