package Compiler.SyntaxParser.Statement;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;
import lowlevel.*;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Statement.java
  **/
public class Statement {
    private Statement statement;
    
    public Statement() {
        this(null);
    }
    
    public Statement(Statement statement) {
        this.statement = statement;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Statement {");
        
        this.statement.print(childIndent);
        
        printHelper(indent, "}");
    }
    
    public void genLLCode(Function function) {
        throw new LowLevelException("Should not call genLLCode on generic statement");
    }
    
    public void genLLCode(Function function, BasicBlock returnBlock) {
        throw new LowLevelException("Should not call genLLCode on generic statement");
    }
    
    public void genLLCode(Function function, BasicBlock returnBlock, BasicBlock basicBlock) {
        throw new LowLevelException("Should not call genLLCode on generic statement");
    }
}
