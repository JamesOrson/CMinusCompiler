package Compiler.SyntaxParser;

import Compiler.SyntaxParser.Other.Program;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: SyntaxParserTest.java
  **/

public class SyntaxParserTest {
    private static CMinusSyntaxParser syntaxParser;
            
    public static void main(String[] args) {
        for (Integer i = 2; i < 3; ++i) {
            try {
                syntaxParser = new CMinusSyntaxParser("test" + i + ".c-");
                Program program = syntaxParser.parse();
                syntaxParser.printAST(program);
                syntaxParser.writeToOutputFile("test" + i + ".ast");
            }
            catch (Exception ex) {
                System.out.print(ex.getMessage() + "\n");
            }
            finally {
                continue;
            }
        }
    }
}
