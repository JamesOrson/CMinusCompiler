package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: BinopExpression.java
  **/
public class BinopExpression extends Expression {
    private Expression leftHandSide;
    private String binop;
    private Expression rightHandSide;
    
    public BinopExpression(Expression leftHandSide) {
        this(leftHandSide, null, null);
    }
    
    public BinopExpression(Expression leftHandSide,
                           String binop, 
                           Expression rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.binop = binop;
        this.rightHandSide = rightHandSide;
    }
    
    public void setBinop(String binop) {
        this.binop = binop;
    }
    
    public void setRightHandSide(Expression rightHandSide) {
        this.rightHandSide = rightHandSide;
    }
    
    public Expression getRightHandSide() {
        return this.rightHandSide;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Binop Expression {");
        
        this.leftHandSide.print(childIndent);
        if (this.rightHandSide != null) {
            printHelper(childIndent, this.binop);
            this.rightHandSide.print(childIndent);
        }
        
        printHelper(indent, "}");
    }
}
