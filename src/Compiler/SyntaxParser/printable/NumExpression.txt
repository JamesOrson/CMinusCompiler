package Compiler.SyntaxParser.Expression;

import static Compiler.SyntaxParser.CMinusSyntaxParser.printHelper;

/**
  * @author James Osborne and Jeremy Tiberg
  * File: Numexpression.java
  **/
public class NumExpression extends Expression {
    private Integer num;
    
    public NumExpression(int num) {
        this.num = num;
    }
    
    public void print(int indent) {
        int childIndent = indent + 4;
        printHelper(indent, "Num Expression {");
        
        printHelper(childIndent, num.toString());
        
        printHelper(indent, "}");
    }
}
