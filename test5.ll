(DATA  a)
(FUNCTION  addThem  [(int d) (int e)]
  (BB 2
    (OPER 3 Func_Entry []  [])
  )
  (BB 3
    (OPER 7 Add_I [(r 4)]  [(r 1)(r 2)])
    (OPER 10 Mov [(r 3)]  [(r 4)])
    (OPER 12 Mov [(m RetReg)]  [(r 3)])
    (OPER 13 Jmp []  [(bb 4)])
  )
  (BB 4
    (OPER 4 Func_Exit []  [])
    (OPER 5 Return []  [(m RetReg)])
  )
)
(FUNCTION  putDigit  [(int s)]
  (BB 2
    (OPER 3 Func_Entry []  [])
  )
  (BB 3
    (OPER 8 Mov [(r 3)]  [(i 48)])
    (OPER 7 Add_I [(r 2)]  [(r 3)(r 1)])
    (OPER 6 Pass []  [(r 2)] [(PARAM_NUM 0)])
    (OPER 11 JSR []  [(s putchar)] [(numParams 1)])
  )
  (BB 4
    (OPER 4 Func_Exit []  [])
    (OPER 5 Return []  [(m RetReg)])
  )
)
(FUNCTION  printInt  [(int r)]
  (BB 2
    (OPER 3 Func_Entry []  [])
  )
  (BB 3
    (OPER 7 Mov [(r 4)]  [(i 0)])
    (OPER 9 Mov [(r 3)]  [(r 4)])
    (OPER 12 Mov [(r 6)]  [(i 10000)])
    (OPER 10 GTE [(r 5)]  [(r 1)(r 6)])
    (OPER 14 BEQ []  [(r 5)(i 0)(bb 8)])
  )
  (BB 6
  )
  (BB 9
    (OPER 16 Mov [(r 7)]  [(i 45)])
    (OPER 15 Pass []  [(r 7)] [(PARAM_NUM 0)])
    (OPER 18 JSR []  [(s putchar)] [(numParams 1)])
    (OPER 20 Mov [(r 8)]  [(i 1)])
    (OPER 19 Pass []  [(r 8)] [(PARAM_NUM 0)])
    (OPER 22 JSR []  [(s putDigit)] [(numParams 1)])
    (OPER 23 Jmp []  [(bb 7)])
  )
  (BB 7
  )
  (BB 4
    (OPER 4 Func_Exit []  [])
    (OPER 5 Return []  [(m RetReg)])
  )
  (BB 15
    (OPER 91 Mov [(r 28)]  [(i 1)])
    (OPER 89 EQ [(r 27)]  [(r 3)(r 28)])
    (OPER 93 BEQ []  [(r 27)(i 0)(bb 18)])
  )
  (BB 17
  )
  (BB 19
    (OPER 95 Mov [(r 29)]  [(i 0)])
    (OPER 94 Pass []  [(r 29)] [(PARAM_NUM 0)])
    (OPER 97 JSR []  [(s putDigit)] [(numParams 1)])
    (OPER 98 Jmp []  [(bb 18)])
  )
  (BB 18
    (OPER 99 Jmp []  [(bb 14)])
  )
  (BB 22
    (OPER 125 Mov [(r 38)]  [(i 1)])
    (OPER 123 EQ [(r 37)]  [(r 3)(r 38)])
    (OPER 127 BEQ []  [(r 37)(i 0)(bb 25)])
  )
  (BB 24
  )
  (BB 26
    (OPER 129 Mov [(r 39)]  [(i 0)])
    (OPER 128 Pass []  [(r 39)] [(PARAM_NUM 0)])
    (OPER 131 JSR []  [(s putDigit)] [(numParams 1)])
    (OPER 132 Jmp []  [(bb 25)])
  )
  (BB 25
    (OPER 133 Jmp []  [(bb 21)])
  )
  (BB 8
    (OPER 26 Mov [(r 10)]  [(i 1000)])
    (OPER 24 GTE [(r 9)]  [(r 1)(r 10)])
    (OPER 28 BEQ []  [(r 9)(i 0)(bb 11)])
  )
  (BB 10
  )
  (BB 12
    (OPER 32 Mov [(r 12)]  [(i 1000)])
    (OPER 30 Div_I [(r 11)]  [(r 1)(r 12)])
    (OPER 34 Mov [(r 2)]  [(r 11)])
    (OPER 35 Pass []  [(r 2)] [(PARAM_NUM 0)])
    (OPER 37 JSR []  [(s putDigit)] [(numParams 1)])
    (OPER 39 Mov [(r 13)]  [(i 32)])
    (OPER 38 Pass []  [(r 13)] [(PARAM_NUM 0)])
    (OPER 41 JSR []  [(s putchar)] [(numParams 1)])
    (OPER 47 Mov [(r 16)]  [(i 1000)])
    (OPER 45 Mul_I [(r 15)]  [(r 2)(r 16)])
    (OPER 43 Sub_I [(r 14)]  [(r 1)(r 15)])
    (OPER 49 Mov [(r 1)]  [(r 14)])
    (OPER 50 Pass []  [(r 1)] [(PARAM_NUM 0)])
    (OPER 52 JSR []  [(s printInt)] [(numParams 1)])
    (OPER 54 Mov [(r 17)]  [(i 32)])
    (OPER 53 Pass []  [(r 17)] [(PARAM_NUM 0)])
    (OPER 56 JSR []  [(s putchar)] [(numParams 1)])
    (OPER 58 Mov [(r 18)]  [(i 1)])
    (OPER 60 Mov [(r 3)]  [(r 18)])
    (OPER 61 Jmp []  [(bb 11)])
  )
  (BB 11
    (OPER 64 Mov [(r 20)]  [(i 100)])
    (OPER 62 GTE [(r 19)]  [(r 1)(r 20)])
    (OPER 66 BEQ []  [(r 19)(i 0)(bb 15)])
  )
  (BB 13
  )
  (BB 16
    (OPER 70 Mov [(r 22)]  [(i 100)])
    (OPER 68 Div_I [(r 21)]  [(r 1)(r 22)])
    (OPER 72 Mov [(r 2)]  [(r 21)])
    (OPER 73 Pass []  [(r 2)] [(PARAM_NUM 0)])
    (OPER 75 JSR []  [(s putDigit)] [(numParams 1)])
    (OPER 81 Mov [(r 25)]  [(i 100)])
    (OPER 79 Mul_I [(r 24)]  [(r 2)(r 25)])
    (OPER 77 Sub_I [(r 23)]  [(r 1)(r 24)])
    (OPER 83 Mov [(r 1)]  [(r 23)])
    (OPER 85 Mov [(r 26)]  [(i 1)])
    (OPER 87 Mov [(r 3)]  [(r 26)])
    (OPER 88 Jmp []  [(bb 14)])
  )
  (BB 14
    (OPER 102 Mov [(r 31)]  [(i 10)])
    (OPER 100 GTE [(r 30)]  [(r 1)(r 31)])
    (OPER 104 BEQ []  [(r 30)(i 0)(bb 22)])
  )
  (BB 20
  )
  (BB 23
    (OPER 108 Mov [(r 33)]  [(i 10)])
    (OPER 106 Div_I [(r 32)]  [(r 1)(r 33)])
    (OPER 110 Mov [(r 2)]  [(r 32)])
    (OPER 111 Pass []  [(r 2)] [(PARAM_NUM 0)])
    (OPER 113 JSR []  [(s putDigit)] [(numParams 1)])
    (OPER 119 Mov [(r 36)]  [(i 10)])
    (OPER 117 Mul_I [(r 35)]  [(r 2)(r 36)])
    (OPER 115 Sub_I [(r 34)]  [(r 1)(r 35)])
    (OPER 121 Mov [(r 1)]  [(r 34)])
    (OPER 122 Jmp []  [(bb 21)])
  )
  (BB 21
    (OPER 134 Pass []  [(r 1)] [(PARAM_NUM 0)])
    (OPER 136 JSR []  [(s putDigit)] [(numParams 1)])
    (OPER 137 Jmp []  [(bb 7)])
  )
)
(FUNCTION  main  []
  (BB 2
    (OPER 3 Func_Entry []  [])
  )
  (BB 3
    (OPER 7 Mov [(r 1)]  [(i 50)])
    (OPER 6 Pass []  [(r 1)] [(PARAM_NUM 0)])
    (OPER 9 JSR []  [(s printInt)] [(numParams 1)])
    (OPER 10 Mov [(r 2)]  [(i 0)])
    (OPER 12 Mov [(m RetReg)]  [(r 2)])
    (OPER 13 Jmp []  [(bb 4)])
  )
  (BB 4
    (OPER 4 Func_Exit []  [])
    (OPER 5 Return []  [(m RetReg)])
  )
)
