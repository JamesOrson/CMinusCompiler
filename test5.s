.data
.comm	a,4,4

.text
	.align 4
.globl  addThem
addThem:
addThem_bb2:
	movl	%EDI, %EAX
	movl	%ESI, %EDI
addThem_bb3:
	addl	%EDI, %EAX
addThem_bb4:
	ret
.globl  putDigit
putDigit:
putDigit_bb2:
putDigit_bb3:
	movl	$48, %EAX
	addl	%EDI, %EAX
	movl	%EAX, %EDI
	call	putchar
putDigit_bb4:
	ret
.globl  printInt
printInt:
printInt_bb2:
	pushq	%R14
	pushq	%R15
	movl	%EDI, %R15D
printInt_bb3:
	movl	$0, %EAX
	movl	%EAX, %R14D
	movl	$10000, %EAX
	cmpl	%EAX, %R15D
	jl	printInt_bb8
printInt_bb9:
	movl	$45, %EAX
	movl	%EAX, %EDI
	call	putchar
	movl	$1, %EAX
	movl	%EAX, %EDI
	call	putDigit
printInt_bb4:
	popq	%R15
	popq	%R14
	ret
printInt_bb15:
	movl	$1, %EAX
	cmpl	%EAX, %R14D
	jne	printInt_bb14
printInt_bb19:
	movl	$0, %EAX
	movl	%EAX, %EDI
	call	putDigit
	jmp	printInt_bb14
printInt_bb22:
	movl	$1, %EAX
	cmpl	%EAX, %R14D
	jne	printInt_bb21
printInt_bb26:
	movl	$0, %EAX
	movl	%EAX, %EDI
	call	putDigit
	jmp	printInt_bb21
printInt_bb8:
	movl	$1000, %EAX
	cmpl	%EAX, %R15D
	jl	printInt_bb11
printInt_bb12:
	movl	$1000, %EDI
	movl	$0, %EDX
	movl	%R15D, %EAX
	idivl	%EDI, %EAX
	movl	%EAX, %R14D
	movl	%R14D, %EDI
	call	putDigit
	movl	$32, %EAX
	movl	%EAX, %EDI
	call	putchar
	movl	$1000, %EDI
	movl	%R14D, %EAX
	imull	%EDI, %EAX
	movl	%R15D, %EDI
	subl	%EAX, %EDI
	movl	%EDI, %R15D
	movl	%R15D, %EDI
	call	printInt
	movl	$32, %EAX
	movl	%EAX, %EDI
	call	putchar
	movl	$1, %EAX
	movl	%EAX, %R14D
printInt_bb11:
	movl	$100, %EAX
	cmpl	%EAX, %R15D
	jl	printInt_bb15
printInt_bb16:
	movl	$100, %EDI
	movl	$0, %EDX
	movl	%R15D, %EAX
	idivl	%EDI, %EAX
	movl	%EAX, %R14D
	movl	%R14D, %EDI
	call	putDigit
	movl	$100, %EDI
	movl	%R14D, %EAX
	imull	%EDI, %EAX
	movl	%R15D, %EDI
	subl	%EAX, %EDI
	movl	%EDI, %R15D
	movl	$1, %EAX
	movl	%EAX, %R14D
printInt_bb14:
	movl	$10, %EAX
	cmpl	%EAX, %R15D
	jl	printInt_bb22
printInt_bb23:
	movl	$10, %EDI
	movl	$0, %EDX
	movl	%R15D, %EAX
	idivl	%EDI, %EAX
	movl	%EAX, %R14D
	movl	%R14D, %EDI
	call	putDigit
	movl	$10, %EDI
	movl	%R14D, %EAX
	imull	%EDI, %EAX
	movl	%R15D, %EDI
	subl	%EAX, %EDI
	movl	%EDI, %R15D
printInt_bb21:
	movl	%R15D, %EDI
	call	putDigit
	jmp	printInt_bb4
.globl  main
main:
main_bb2:
main_bb3:
	movl	$50, %EAX
	movl	%EAX, %EDI
	call	printInt
	movl	$0, %EAX
main_bb4:
	ret
